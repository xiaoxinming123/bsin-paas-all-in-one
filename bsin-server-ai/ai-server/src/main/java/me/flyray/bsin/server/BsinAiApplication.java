package me.flyray.bsin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import dev.langchain4j.data.segment.TextSegment;
import dev.langchain4j.model.embedding.AllMiniLmL6V2EmbeddingModel;
import dev.langchain4j.model.embedding.BgeSmallZhEmbeddingModel;
import dev.langchain4j.model.embedding.EmbeddingModel;
import dev.langchain4j.retriever.EmbeddingStoreRetriever;
import dev.langchain4j.retriever.Retriever;
import dev.langchain4j.store.embedding.EmbeddingStore;
import dev.langchain4j.store.embedding.milvus.MilvusEmbeddingStore;
import me.flyray.bsin.server.websocket.WebSocketSever;

/**
 * @author ：leonard
 * @date ：Created in 2023/04/21 16:00
 * @description：bsin ai 引擎
 * @modified By：
 */

@ImportResource({"classpath*:sofa/rpc-provider-ai.xml"})
@SpringBootApplication
@ComponentScan("me.flyray.bsin.*")
public class BsinAiApplication {

    @Bean
    EmbeddingModel embeddingModel() {
        // 此处可以选择不同的EmbeddingModel：OpenAiEmbeddingModel AllMiniLmL6V2EmbeddingModel
        return new BgeSmallZhEmbeddingModel();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(BsinAiApplication.class, args);
        WebSocketSever.setApplicationContext(applicationContext);
    }

}
