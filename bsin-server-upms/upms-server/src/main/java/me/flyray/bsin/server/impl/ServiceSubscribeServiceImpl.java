package me.flyray.bsin.server.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import java.util.Map;
import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.context.LoginInfoContextHelper;
import me.flyray.bsin.domain.LoginUser;
import me.flyray.bsin.facade.service.ServiceSubscribeService;
import me.flyray.bsin.server.domain.SysServiceSubscribe;
import me.flyray.bsin.server.mapper.ProductAppMapper;
import me.flyray.bsin.server.mapper.ServiceSubscribeMapper;
import me.flyray.bsin.utils.BsinPageUtil;
import me.flyray.bsin.utils.BsinSnowflake;
import me.flyray.bsin.utils.Pagination;
import me.flyray.bsin.utils.RespBodyHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author leonard
 * @date 2024/01/24
 * @desc 服务和功能订阅CRUD
 */
public class ServiceSubscribeServiceImpl implements ServiceSubscribeService {

  @Autowired private ServiceSubscribeMapper serviceSubscribeMapper;

  @Override
  public Map<String, Object> add(Map<String, Object> requestMap) {
    SysServiceSubscribe serviceSubscribe =
        BsinServiceContext.getReqBodyDto(SysServiceSubscribe.class, requestMap);
    serviceSubscribe.setId(BsinSnowflake.getId());
    serviceSubscribeMapper.insert(serviceSubscribe);
    return RespBodyHandler.setRespBodyDto(serviceSubscribe);
  }

  @Override
  public Map<String, Object> delete(Map<String, Object> requestMap) {
    String id = (String) requestMap.get("id");
    serviceSubscribeMapper.deleteById(id);
    return RespBodyHandler.RespBodyDto();
  }

  @Override
  public Map<String, Object> edit(Map<String, Object> requestMap) {
    SysServiceSubscribe serviceSubscribe =
        BsinServiceContext.getReqBodyDto(SysServiceSubscribe.class, requestMap);
    serviceSubscribeMapper.updateById(serviceSubscribe);
    return RespBodyHandler.setRespBodyDto(serviceSubscribe);
  }

  @Override
  public Map<String, Object> getList(Map<String, Object> requestMap) {
    String customerNo = (String) requestMap.get("customerNo");
    List<SysServiceSubscribe> serviceSubscribeList = serviceSubscribeMapper.selectList(customerNo);
    return RespBodyHandler.setRespBodyListDto(serviceSubscribeList);
  }

  @Override
  public Map<String, Object> getPageList(Map<String, Object> requestMap) {
    LoginUser loginUser = LoginInfoContextHelper.getLoginUser();
    String tenantId = (String) requestMap.get("tenantId");
    String merchantNo = (String) requestMap.get("merchantNo");
    String customerNo = (String) requestMap.get("customerNo");
    Pagination pagination = (Pagination) requestMap.get("pagination");
    BsinPageUtil.pageNotNull(pagination);
    PageHelper.startPage(pagination.getPageNum(), pagination.getPageSize());
    List<SysServiceSubscribe> serviceSubscribeList = serviceSubscribeMapper.selectList(customerNo);
    PageInfo<SysServiceSubscribe> pageInfo =
        new PageInfo<SysServiceSubscribe>(serviceSubscribeList);
    return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
  }
}
