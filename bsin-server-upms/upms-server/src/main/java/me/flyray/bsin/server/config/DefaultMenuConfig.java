package me.flyray.bsin.server.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

@Data
@ConfigurationProperties(prefix = "me.flyray.bsin.default-menu")
@PropertySource(value="classpath:application.properties", encoding="UTF-8")
public class DefaultMenuConfig {

    private String appId;

    private String menuName;

    private Integer sort;

    private Integer type;

    private String permission;

    private String path;

    private String parentId;

    private String roleName;

}
