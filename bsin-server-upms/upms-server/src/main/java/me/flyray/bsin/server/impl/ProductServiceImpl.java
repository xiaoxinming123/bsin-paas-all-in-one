package me.flyray.bsin.server.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.ProductService;
import me.flyray.bsin.server.domain.SysApp;
import me.flyray.bsin.server.domain.SysDict;
import me.flyray.bsin.server.domain.SysDictItem;
import me.flyray.bsin.server.domain.SysProduct;
import me.flyray.bsin.server.domain.SysProductApp;
import me.flyray.bsin.server.mapper.ProductAppMapper;
import me.flyray.bsin.server.mapper.ProductMapper;
import me.flyray.bsin.utils.BsinPageUtil;
import me.flyray.bsin.utils.BsinSnowflake;
import me.flyray.bsin.utils.Pagination;
import me.flyray.bsin.utils.RespBodyHandler;

/**
 * @author bolei
 * @date 2023/11/5
 * @desc
 */

public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductAppMapper productAppMapper;

    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        SysProduct product = BsinServiceContext.getReqBodyDto(SysProduct.class, requestMap);
        String id = BsinSnowflake.getId();
        product.setProductId(id);
        productMapper.insert(product);
        return RespBodyHandler.setRespBodyDto(product);
    }

    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String id = (String)requestMap.get("id");
        productMapper.deleteById(id);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> edit(Map<String, Object> requestMap) {
        SysProduct product = BsinServiceContext.getReqBodyDto(SysProduct.class, requestMap);
        productMapper.updateById(product);
        return RespBodyHandler.setRespBodyDto(product);
    }

    @Override
    public Map<String, Object> getList(Map<String, Object> requestMap) {
        String productName = (String)requestMap.get("productName");
        List<SysProduct> productList = productMapper.selectList(productName);
        return RespBodyHandler.setRespBodyListDto(productList);
    }

    @Override
    public Map<String, Object> getPageList(Map<String, Object> requestMap) {
        String productName = (String)requestMap.get("productName");
        Pagination pagination = (Pagination) requestMap.get("pagination");
        BsinPageUtil.pageNotNull(pagination);
        PageHelper.startPage(pagination.getPageNum(),pagination.getPageSize());
        List<SysProduct> productList = productMapper.selectList(productName);
        PageInfo<SysProduct> pageInfo = new PageInfo<SysProduct>(productList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

    @Override
    public Map<String, Object> addProductApp(Map<String, Object> requestMap) {
        SysProductApp productApp = BsinServiceContext.getReqBodyDto(SysProductApp.class, requestMap);
        String id = BsinSnowflake.getId();
        // 判断应用是否已经存在
        SysProductApp productAppResult = productAppMapper.selectByProductIdAndAppId(productApp.getProductId(),productApp.getAppId());
        if(productAppResult != null){
            throw new BusinessException("应用已经添加");
        }
        productApp.setId(id);
        productAppMapper.insert(productApp);
        return RespBodyHandler.setRespBodyDto(productApp);
    }

    @Override
    public Map<String, Object> deleteProductApp(Map<String, Object> requestMap) {
        String productId = (String)requestMap.get("productId");
        String appId = (String)requestMap.get("id");
        productAppMapper.deleteById(productId, appId);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> getProductAppList(Map<String, Object> requestMap) {
        String productId = (String)requestMap.get("productId");
        List<SysApp> sysApps = productAppMapper.selectListByProductId(productId);
        return RespBodyHandler.setRespBodyListDto(sysApps);
    }

    @Override
    public Map<String, Object> getProductAppFunctionList(Map<String, Object> requestMap) {
        String productId = (String)requestMap.get("productId");
        List<SysApp> sysApps = productAppMapper.selectListByProductId(productId);
        // 查询应用的功能

        return RespBodyHandler.setRespBodyListDto(sysApps);
    }

    @Override
    public Map<String, Object> getProductAppPageList(Map<String, Object> requestMap) {
        String productId = (String)requestMap.get("productId");
        Pagination pagination = (Pagination) requestMap.get("pagination");
        BsinPageUtil.pageNotNull(pagination);
        PageHelper.startPage(pagination.getPageNum(),pagination.getPageSize());
        List<SysApp> appList = productAppMapper.selectPageList(productId);
        PageInfo<SysApp> pageInfo = new PageInfo<SysApp>(appList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

}
