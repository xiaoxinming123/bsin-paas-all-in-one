package me.flyray.bsin.utils;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.pagehelper.PageInfo;
import lombok.Builder;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author ：leonard
 * @date ：Created in 2024/1/20 18:12
 * @description：请求数据
 * @modified By：
 */
@Builder
@Data
public class ReqBodyHandler {
  private String serviceName;
  private String methodName;
  private String version;
  Object bizParams;
}
